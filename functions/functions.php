<?php

    include_once('creds.php');

    function connection() 
    {
        $conn = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME); 
        if($conn->connect_errno > 0){
            echo('Unable to connect to database [' .$conn->connect_error . ']');
        }
        return $conn;
    }
    function get_all_items($type)
    {
        $db = connection();
        $sql = 'SELECT * FROM tbl_consumables WHERE CATEGORY = "' . $type . '"';
        $arrayget = [];

        $result = $db->query($sql);
        
        if(!$result)
        {
            echo ('There was an error running the query [' . $db->error . ']');
        }
        while($row = $result->fetch_assoc())
        {
            $arrayget[] = array 
            (
                'id' => $row['ID'],
                'name' => $row['NAME'],
                'price' => $row['PRICE'],
                'category' => $row['CATEGORY']
            );
        }

        $json = json_encode(array($arrayget));

        $result->free();
        $db->close();

        return $json;
    }
    function show_all_items($data, $page) 
    {

        $arrayshow = [];

        $arrayshow = json_decode($data, TRUE);
        
        $output = "";
        
        if (count($arrayshow[0]) > 0 ) 
        {
            for ($i = 0; $i < count($arrayshow[0]); $i++) 
            {
                if ($page == "index") 
                {
                    $output .= "<tr><td>".$arrayshow[0][$i]['name']."</td><td>".$arrayshow[0][$i]['price']."</td></tr>"; 
                }
                if ($page == "admin") 
                {
                    $output .= "<tr><td>".$arrayshow[0][$i]['name']."</td><td>".$arrayshow[0][$i]['price']."</td><td><a href=\"edit.php\">Edit</a></td><td><a href=\"delete.php\">Delete</a></td></tr>"; 
                } 
            }
        return $output;
        }
        else 
        {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            return $output;
        }
    }
    function loadData($id) {

        $db = connection();
        $sql = 'SELECT * FROM tbl_consumables WHERE ID = $id';
        $array = [];

        $result = $db->query($sql);
        
        if(!$result)
        {
            echo ('There was an error running the query [' . $db->error . ']');
        }
        while($row = $result->fetch_assoc())
        {
            $array[] = array 
            (
                'id' => $row['ID'],
                'name' => $row['NAME'],
                'price' => $row['PRICE'],
                'category' => $row['CATEGORY']
            );
        }

        $json = json_encode(array($array));

        $result->free();
        $db->close();

        return $json;
    }

    //Fill in data for forms

    function displayID()
    {
        $id = $_GET['id'];
        $array = json_decode(loadData($id), True);
        return $array[0]['id'];
    }

    function displayName()
    {
        $id = $_GET['id'];
        $array = json_decode(loadData($id), True);
        return $array[0]['name'];
    }

    function displayPrice()
    {
        $id = $_GET['id'];
        $array = json_decode(loadData($id), True);
        return $array[0]['price'];
    }

    // function displayCategory()
    // {
    //     $id = $_GET['id'];
    //     $array = json_decode(loadData($id), True);
    //     return $array[0]['category'];
    // } 
    function editRecord() {

        if(isset($_POST['updateForm']))
        {
            $db = connection();
            $name = $db->real_escape_string($_POST['name']);
            $price = $db->real_escape_string($_POST['price']);
            $category = $db->real_escape_string($_POST['category']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_people SET NAME='".$name."', PRICE='".$price."', CATEGORY='".$category."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }
    function redirect($location)
    {
        $URL = $location;
        echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
        echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
        exit();
    }
    function loginAdmin() 
    {

        if(isset($_POST['login']))
        {
            $db = connection();

            $user = $db->real_escape_string($_POST['username']);
            $pass = $db->real_escape_string($_POST['password']);

            $sql = "SELECT * FROM tbl_admin WHERE NAME = '$user' && PASSWRD = '$pass'";
            $arr = [];

            $result = $db->query($sql);

            if(!$result) 
            {
                die("There was an error running the query [".$db->error."] ");
            }
            
            while ($row = $result->fetch_assoc()) 
            {
                $arr[] = array 
                (
                    "user" => $row['NAME'],
                    "pass" => $row['PASSWRD']
                );
            }

            $json = json_encode(array($arr));
            
            $result->free();
            $db->close();

            if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
            {
                $_SESSION['login'] = TRUE;
                redirect("admin/index.php");
            }
            else
            {
                $_SESSION['login'] = FALSE;
                echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
                redirect("wrong.html");
            }
            
        }
    }

    ////////////////////////////////////////////////////////////////
    /////////              Logout Admin               //////////////
    ////////////////////////////////////////////////////////////////

    function logout()
    {
        if(isset($_POST['logout']))
        {
            session_start();

            // Unset all of the session variables.
            $_SESSION = array();

            // If it's desired to kill the session, also delete the session cookie.
            // Note: This will destroy the session, and not just the session data!

            if (ini_get("session.use_cookies")) 
            {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                );
            }

            // Finally, destroy the session.
            session_destroy();

            redirect("../index.php");
        }
    }
 ?>